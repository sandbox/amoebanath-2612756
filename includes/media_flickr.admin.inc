<?php

/**
 * @file
 * Admin page callbacks for the Media: Flickr module.
 */

/**
 * Form builder; The Media: Flickr configuration form.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function media_flickr_settings_form() {
  $form['media_flickr_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Flickr Username'),
    '#default_value' => variable_get('media_flickr_username', ''),
  );
  $form['media_flickr_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Flickr API key'),
    '#default_value' => variable_get('media_flickr_api_key', ''),
  );
  $form['media_flickr_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Flickr API secret'),
    '#default_value' => variable_get('media_flickr_api_secret', ''),
  );


  if (variable_get('media_flickr_token_secret', NULL)) {
    $form['media_flickr_connect_global_account_link'] = array(
      '#markup' => '<p>' . t('If you\'re experiencing issues, <a href="@link"> reconnect your Flickr account</a>', array(
          '@link' => url('connect/oauthconnector_flickr', array(
            'query' =>
              drupal_get_destination(),
            )
          ),
        )) . '</p>',
    );
    $form['media_flickr_remove_global_account_link'] = array(
      '#markup' => '<p>' . l(t('Remove the connected Flickr account'), 'admin/config/media/media-flickr/disconnect-account', array(
          'query' => array(
            'token' => drupal_get_token('media_flickr'),
          ),
        )) . '</p>',
    );
  }
  else {
    $form['media_flickr_connect_global_account_link'] = array(
      '#markup' => '<p>' . t('<a href="@link">Connect a Flickr account</a> to access private photos', array(
          '@link' => url('connect/oauthconnector_flickr'),
        )) . '</p>',
    );
  }

  return system_settings_form($form);
}

/**
 * Disconnect and remove details for the connected Flickr account.
 */
function media_flickr_admin_disconnect_account() {
  if (isset($_GET['token']) && drupal_valid_token($_GET['token'], 'media_flickr')) {
    $variables = array('media_flickr_consumer_key', 'media_flickr_consumer_secret', 'media_flickr_token_key', 'media_flickr_token_secret', 'media_flickr_username', 'media_flickr_nsid');
    foreach($variables as $variable) {
      variable_del($variable);
    }
    drupal_set_message(t('Account deleted.'), 'status');
    drupal_goto('admin/config/media/media-flickr');
  }
  else {
    return MENU_ACCESS_DENIED;
  }
}
